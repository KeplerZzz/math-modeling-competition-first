%% 加载数据
clc;clear
load data.mat


[n,m] = size(X);
%% 对数据先进行简单的标准化
Z = X ./ repmat(sum(X.*X) .^ 0.5, n, 1);
disp('标准化矩阵 Z = ')
disp(Z)


if sum(sum(Z<0)) >0   % 如果之前标准化后的Z矩阵中存在负数，则重新对X进行标准化
    disp('原来标准化得到的Z矩阵中存在负数，所以需要对X重新标准化')
    for i = 1:n
        for j = 1:m
            Z(i,j) = [X(i,j) - min(X(:,j))] / [max(X(:,j)) - min(X(:,j))];
        end
    end
     weight = Entropy_Method(Z);
end
       
    
%% 对各个权重进行排序，index_w为权重排序后的所确定的对325个样本影响较大的变量序号排名
% 前25个为：[165;351;200;105;271;344;263;96;84;265;224;213;88;7;315;273;300;232;39;325;322;313;248;98;184]
% delete:222,211,214,36
[sorted_w,index_w] = sort(weight,'descend');
index_w = index_w';


index_w([3,5,6,7]) = []; 
index_w(14) = [];
l = X(:,index_w(1:24));


