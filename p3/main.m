clc;clear
load rank25.mat;
load Y.mat;
load net2.mat;

[n,m] = size(X);
%% 对样本进行标准化处理
Z = X ./ repmat(sum(X.*X) .^ 0.5, n, 1);

%% 不存在负数的标准化处理
% for i = 1:n
%     for j = 1:m
%         Z(i,j) = [X(i,j) - min(X(:,j))] / [max(X(:,j)) - min(X(:,j))];
% 	end
% end

index = randperm(325);
train_index = index(1:216);
test_index = index(217:end);

train_X = Z(train_index,:);
train_Y = Y(1:216);

test_X = Z(test_index,:);
test_Y = Y(217:end);

predict_Y = zeros(109,1);
for i = 1:109
    result = sim(net,test_X(i,:)');
    predict_Y(i) = result;
end
disp('预测值为：');
disp(predict_Y);

mean_error = sum(abs(predict_Y - test_Y) ./ test_Y * 100) / size(test_Y,1);


plot_index = 10:20;
figure(1);
plot([1:11],test_Y(plot_index),'*b-',[1:11],predict_Y(plot_index),'o-')
axis([1 11 0 2]);
hold on
legend('准确值','预测值')
xlabel('测试数据');
ylabel('汽油中的辛烷值');

figure(2);
predict_error = (predict_Y - test_Y) ./ test_Y * 100;
plot([1:11],predict_error(plot_index),'+-');
hold on 
xlabel('测试数据');
ylabel('预测误差/%');
axis([1 11 -20 20]);

