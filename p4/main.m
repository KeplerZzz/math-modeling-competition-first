5
%% 使用蒙特卡罗模拟进行优化测试
% 根据B题专家的提示：提升率计算是按这样来的： 
%（原有参考的损失值-调整主要操作变量的通过已生成模型预测的损失值） /（原料辛烷值-产品辛烷值）这里需要超过30%才能说是调整成功。
clc;clear
load operation_range.mat;
load rank25.mat;
load S_content.mat;
load net2.mat;
load loss_RON.mat;
format long g

%% 筛选出产品硫含量超过5ug/g的样本序号
index_S_more_than_5 = find((S_content > 5.0));
disp('产品硫含量超过5ug/g的样本序号为：')
disp(index_S_more_than_5');

%% 在原有的325个样本中删除产品硫含量超过5ug/g的样本
X_S_less_5 = X;
X_S_less_5(index_S_more_than_5,:) = [];
loss_RON(index_S_more_than_5) = []; 

%% 由于题目要求只改变主要操作变量所以对模型的第一个特征原料的辛烷值进行保留
X_RON = X_S_less_5(:,1);

%% 使用蒙特卡罗模拟
n = 100000; % 蒙特卡洛模拟的次数
% 生成蒙特卡洛模拟的 n 个样本
k = 6; % k代表模拟产品硫含量低于5ug/g的样本的序号
predict_x = zeros(n,25);
for i = 1:size(predict_x,2)
    if i == 1
        predict_x(:,i) = X_S_less_5(k,i);
    else
        predict_x(:,i) = unifrnd(operation_min(i-1),operation_max(i-1),n,1);
    end
end


%% 每次预测前先将样本放入原有的325个数据样本中，进行标准化
min_loss_RON = loss_RON(k);
best_X_index = 0;
for i = 1:n
    X(326,:) = predict_x(i,:);
    z = X ./ repmat(sum(X.*X) .^ 0.5, size(X,1), 1);
    predict_Y = sim(net,z(326,:)');
    if predict_Y < min_loss_RON
        best_X_index = i;
        min_loss_RON = predict_Y;
    end
end


if best_X_index == 0
    disp('不存在较优解!')
else
    disp(['得到最佳的辛烷损失的预测值为：' num2str(min_loss_RON)]);
    disp(['最优的特征出现在第' num2str(best_X_index) '次蒙特卡罗模拟']);
    disp('最优的特征为：')
    disp(predict_x(best_X_index,:));
end



