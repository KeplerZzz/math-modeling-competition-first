function g_error = getRestError(x,m)
    n = size(m,1);
    g_error = zeros(size(x,1),size(x,2));
    for i = 1:n
        g_error(:,i) = x(:,i) - m(i);
    end
end