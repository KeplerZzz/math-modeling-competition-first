%% 得到的结果
%（修改后，没有用）
% 针对样本序号为 285 的数据进行处理
% 最终被剔除的变量序号为：25,46,49,74,84,96,110,111,139,151,205,208,216,245,271,272,273,279,299,332  
% 针对样本需要为 313 的数据进行处理
% 最终被剔除的变量序号为：22,25,46,49,84,96,111,151,162,169,205,208,216,245,271,272,273,279,299

clc;clear
load data.mat
%% 处理范围字符串，提取指标的上界和下界
range_split_index = zeros(354,1);
for i = 1:size(str_range,1)
    char_range = char(str_range(i));
    index_split = find(char_range == '-');
    if length(index_split) == 1
        range_split_index(i) = index_split(1);
        char_range(index_split(1)) = '/';
    elseif length(index_split) == 2
        range_split_index(i) = index_split(2);
        char_range(index_split(2)) = '/';
    elseif length(index_split) == 3
        range_split_index(i) = index_split(2);
        char_range(index_split(2)) = '/';
    end
    str_range(i) = char_range;
end

dealed_str_range = split(str_range, '/');
min = zeros(354,1);
max = zeros(354,1);
for i = 1:size(dealed_str_range,1)
    char_range = char(str_range(i));
    min(i) = str2double(dealed_str_range(i,1));
    max(i) = str2double(dealed_str_range(i,2));
end

%% 处理data285

%先对缺失值（0值）进行处理
% for i = 1:size(data285,1)
%     for j = 1:size(data285,2)
%         if data285(i,j) == 0
%             data285(i,j) = inf;
%         end
%     end
% end

for i = 1:size(data285,1)
    for j = 1:size(data285,2)
        if data285(i,j) < min(j)
            data285(i,j) = inf;
        elseif data285(i,j) > max(j)
            data285(i,j) = inf;
        end
    end
end


%% 处理缺失值
precent_of_missing_285 = sum(abs(data285) == inf) ./ size(data285,1);
% 找到缺失值超过50%的下标，将数据剔除
index_missing_285_first = find(precent_of_missing_285>0.5);
data285(:,index_missing_285_first) = [];

% 得到缺失值不超过50%的下标，用均值填充
index_mean_change = find(sum(abs(data285) == inf) > 0);
index_mean_change = index_mean_change';
for i = 1:size(index_mean_change,1)
    data_not_inf = data285(:,index_mean_change(i));
    index_data_not_inf = find(data_not_inf ~= inf);
    sum_data_not_inf = sum(data_not_inf(index_data_not_inf));
    mean_data_not_inf = sum_data_not_inf / size(index_data_not_inf,1);
    index_data_inf = find(abs(data_not_inf) == inf);
    data285(index_data_inf,index_mean_change(i)) = mean_data_not_inf;
end

% delta285 = layida(data285);
% mean285 = getmean(data285);
% rest_error285 = getRestError(data285,mean285);
% 
% for i = 1:size(rest_error285,1)
%     for j = 1:size(rest_error285,2)
%         if abs(rest_error285(i,j)) > 3*delta285(j)
%             data285(i,j) = inf;
%         end
%     end
% end
% 
% sum(data285 == inf)
% 
% precent_of_missing_285 = sum(abs(data285) == inf) ./ size(data285,1);
% % 找到缺失值超过50%的下标，将数据剔除
% index_missing_285_second = find(precent_of_missing_285>0.5);
% data285(:,index_missing_285_second) = [];
% 
% % 得到缺失值不超过50%的下标，用均值填充
% index_mean_change = find(sum(abs(data285) == inf) > 0);
% index_mean_change = index_mean_change';
% for i = 1:size(index_mean_change,1)
%     data_not_inf = data285(:,index_mean_change(i));
%     index_data_not_inf = find(data_not_inf ~= inf);
%     sum_data_not_inf = sum(data_not_inf(index_data_not_inf));
%     mean_data_not_inf = sum_data_not_inf / size(index_data_not_inf,1);
%     index_data_inf = find(abs(data_not_inf) == inf);
%     data285(index_data_inf,index_mean_change(i)) = mean_data_not_inf;
% end



%% 处理data313
% for i = 1:size(data313,1)
%     for j = 1:size(data313,2)
%         if data313(i,j) == 0
%             data313(i,j) = inf;
%         end
%     end
% end


for i = 1:size(data313,1)
    for j = 1:size(data313,2)
        if data313(i,j) < min(j)
            data313(i,j) = inf;
        elseif data313(i,j) > max(j)
            data313(i,j) = inf;
        end
    end
end

precent_of_missing_313 = sum(abs(data313) == inf) ./ size(data313,1);
% 找到缺失值超过50%的下标，将数据剔除
index_missing_313_first = find(precent_of_missing_313>0.5);
data313(:,index_missing_313_first) = [];



% 得到缺失值不超过50%的下标，用均值填充
index_mean_change = find(sum(abs(data313) == inf) > 0);
index_mean_change = index_mean_change';
for i = 1:size(index_mean_change,1)
    data_not_inf = data313(:,index_mean_change(i));
    index_data_not_inf = find(data_not_inf ~= inf);
    sum_data_not_inf = sum(data_not_inf(index_data_not_inf));
    mean_data_not_inf = sum_data_not_inf / size(index_data_not_inf,1);
    index_data_inf = find(abs(data_not_inf) == inf);
    data313(index_data_inf,index_mean_change(i)) = mean_data_not_inf;
end

% delta313 = layida(data313);
% mean313 = getmean(data313);
% rest_error313 = getRestError(data313,mean313);
% 
% for i = 1:size(rest_error313,1)
%     for j = 1:size(rest_error313,2)
%         if abs(rest_error313(i,j)) > 3*delta313(j)
%             data313(i,j) = inf;
%         end
%     end
% end
% 
% sum(data313 == inf)
% 
% precent_of_missing_313 = sum(abs(data313) == inf) ./ size(data313,1);
% % 找到缺失值超过50%的下标，将数据剔除
% index_missing_313_second = find(precent_of_missing_313>0.5);
% data313(:,index_missing_313_second) = [];
% 
% % 得到缺失值不超过50%的下标，用均值填充
% index_mean_change = find(sum(abs(data313) == inf) > 0);
% index_mean_change = index_mean_change';
% for i = 1:size(index_mean_change,1)
%     data_not_inf = data313(:,index_mean_change(i));
%     index_data_not_inf = find(data_not_inf ~= inf);
%     sum_data_not_inf = sum(data_not_inf(index_data_not_inf));
%     mean_data_not_inf = sum_data_not_inf / size(index_data_not_inf,1);  
%     index_data_inf = find(abs(data_not_inf) == inf);
%     data313(index_data_inf,index_mean_change(i)) = mean_data_not_inf;
% end

sum(sum(data285 == -inf))
sum(sum(data313 == -inf))


result_285 = getmean(data285)';
result_313 = getmean(data313)';
