function delta = layida(x)
    m = size(x,2);
    n = size(x,1);
    delta = zeros(m,1);
    for i = 1:m
        x_2 = x(:,i) .^ 2;
        sum_x_2 = sum(x_2);
        sumx_2 = sum(x(:,i)) ^ 2;
        delta(i) = sqrt(((sum_x_2 - sumx_2/n)) / (n-1));
    end
end