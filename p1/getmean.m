function g_mean = getmean(x)
    m = size(x,2);
    g_mean = zeros(m,1);
    for i = 1:m
        g_mean(i) = sum(x(:,i)) ./ size(x,1);
    end
end