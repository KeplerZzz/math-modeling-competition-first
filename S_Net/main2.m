%% 使用蒙特卡罗模拟进行优化测试
% 根据B题专家的提示：提升率计算是按这样来的： 
%（原有参考的损失值-调整主要操作变量的通过已生成模型预测的损失值） /（原料辛烷值-产品辛烷值）这里需要超过30%才能说是调整成功。
clc;clear
load operation_range.mat;
load rank25.mat;
load net.mat;
format long g


%% 使用蒙特卡罗模拟
n = 100000; % 蒙特卡洛模拟的次数
% 生成蒙特卡洛模拟的 n 个样本
predict_x = zeros(n,25);
for i = 1:size(predict_x,2)
    if i == 1
        predict_x(:,i) = X(133,i);
    else
        predict_x(:,i) = unifrnd(operation_min(i-1),operation_max(i-1),n,1);
    end
end


%% 每次预测前先将样本放入原有的325个数据样本中，进行标准化
m = 1;
S = [];
best_X_index = [];
for i = 1:n
    X(326,:) = predict_x(i,:);
    z = X ./ repmat(sum(X.*X) .^ 0.5, size(X,1), 1);
    predict_Y = sim(net,z(326,:)');
    if predict_Y < 5
        best_X_index(m) = i;
        S(m) = predict_Y;
        m = m + 1;
    end
end


if size(best_X_index) == 0
    disp('不存在较优解!')
else
    disp(best_X_index);
end



