3
%% 硫含量模型绘图
clc;clear
load operation_range.mat;
load step.mat;
load net.mat;
load best_feature2.mat;
load rank25.mat;
load label_x.mat;

n = size(step,1);
i = 1;
while i ~= 0
    i = input('请输入需要查看操作变量的序号：(序号范围1~24,按0退出)');
    if i == 0
        disp('正在退出');
        break;
    elseif (i>=1) && (i<=24)
        disp('正在绘图，请等待...');
        figure(i);
        x = operation_min(i):step(i):operation_max(i);
        [predict_Y,size_Z] = get_lone_predict_Y(net,best_feature,i+1,x,X);
        label_Y = predict_Y;
        plot(x,label_Y,'b-','LineWidth',2);
        xlabel(label_X(i));
        ylabel('133号样本的产品硫含量（μg/g）');
        disp([x(1),label_Y(1)]);
        disp([x(end),label_Y(end)]);
    else
        disp('输入错误，请重新输入！');
    end
end