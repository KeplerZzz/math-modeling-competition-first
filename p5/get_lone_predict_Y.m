function [predict_Y,size_Z] = get_lone_predict_Y(net,best_feature,index,x,X)
    for i = 1:size(x,2)
        best_feature(index) = x(i);
        X(326,:) = best_feature;
        z = X ./ repmat(sum(X.*X) .^ 0.5, size(X,1), 1);
        predict_Y(i) = sim(net,z(326,:)');
    end
    size_Z = i - 1;
end